import 'jquery';

import 'angular';
import 'angular-ui-router';
import 'angular-material';

import '../assets/main.scss';

import DbService from './services/db';
import Video from './services/video';

import Router from './router';
import Theme from './theme';

import validateVideoUrl from './filters/validateVideoUrl';
import isEmpty from './filters/isEmpty';

import appVideoListItems from './directives/video-list-items/directive';
import appVideoTileItems from './directives/video-tile-items/directive';
import noVideo from './directives/no-video/directive';

import DefaultController from './controllers/default';

import 'angular-material/angular-material.css';

angular
  .module('video-app', ['ngMaterial', 'ui.router'])
  .service('DB', DbService)
  .service('video', Video)
  .filter('validateVideoUrl', validateVideoUrl)
  .filter('isEmpty', isEmpty)
  .directive('noVideo', noVideo)
  .directive('appVideoListItems', appVideoListItems)
  .directive('appVideoTileItems', appVideoTileItems)
  .config(Theme)
  .config(Router);

