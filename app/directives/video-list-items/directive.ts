declare function require(path: string): string;

export default function appVideoListItems() {
  return {
    restrict: 'E',
    template: require('./template.html')
  };
}