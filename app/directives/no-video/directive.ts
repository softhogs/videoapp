declare function require(path: string) : string;

export default function noVideo() {
  return {
    restrict: 'E',
    template: require('./template.html')
  };
}