import _ = require('underscore');

export default function isEmpty() {
  return (data) => {
    return _.isEmpty(data);
  };
};