import {IVideoItem} from './videoItemInterface';
import _ = require('underscore');

interface IStorageVideoItem extends IVideoItem {
  destroy() : void;
}

class StorageVideoItem implements IStorageVideoItem {
  videoId:string;
  type:number;
  thumbnail:string;
  duration:string;
  likes:number;
  plays:number;
  title:string;
  creationDate:number;
  starred:boolean;
  embed:string;

  constructor(public origin: IVideoItem, private storage: DbService) {
    _.extend(this, origin);
    this.storage = storage;
  }

  destroy() {
    this.storage.destroy(<IStorageVideoItem> this);
  }
}

export default class DbService {
  private storage: Storage = localStorage;

  private static PREFIX: string = 'video-app';
  private items : IStorageVideoItem[];

  constructor() {
    try {
      let items = JSON.parse(this.storage.getItem(DbService.PREFIX));
      this.items = Array.isArray(items) ? this.populateRecords(items) : [];
    } catch (e) {
      this.items = [];
    }
  }

  private populateRecords(items : IVideoItem[]) : IStorageVideoItem[] {
    var result : IStorageVideoItem[] = [];

    for (let item of items) {
      result.push(this.populate(item));
    }

    return result;
  }

  private populate(item : IVideoItem) : IStorageVideoItem {
    return new StorageVideoItem(item, this);
  }

  private sync() {
    this.storage.setItem(DbService.PREFIX, JSON.stringify(this.items.map((item: StorageVideoItem) => {
      return item.origin;
    })));
  }

  findAll() : IStorageVideoItem[] {
    return this.items;
  }

  addItem(video: IVideoItem) : void {
    this.items.push(this.populate(video));
    this.sync()
  }

  flush() {
    this.items.length = 0;
    this.sync()
  }

  destroy(item : IStorageVideoItem) : void {
    var index : number = this.items.indexOf(item);

    if (index !== -1) {
      this.items.splice(index, 1);
    }

    this.sync();
  }

}
