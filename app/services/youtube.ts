import {IVideo} from './videoInterface';
import {IVideoItem} from './videoItemInterface';
import {YouTubeValidator, IVideoValidator} from '../validators/validators';

interface YouTubeAuthorization {
  key: string,
  secret: string
}

export default class YouTube implements IVideo<any>
{

  private static URL_VIDEO: string = 'https://www.googleapis.com/youtube/v3/videos?id=$VID$&key=$KEY$&part=snippet,contentDetails,statistics,status';
  private static API_KEY: string = 'AIzaSyB95IZgwpxmGlw9SShGDio_IrRUzF7PlvA';
  private validator: IVideoValidator;
   
  static $inject = ['$http', '$q'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    this.validator = new YouTubeValidator();
  }
  
  /**
   * @param ID
   * @returns {ng.IPromise}
   */
  public fetchVideo(ID: string): ng.IPromise<IVideoItem> {
    var self = this;
    return this.$http
      .get(YouTube.createVideoUrl(this.validator.getId(ID)), {})
      .then((response) :IVideoItem =>{
        var videoData = response.data['items'][0];
        return <IVideoItem>{
          videoId: videoData.id,
          type: 1,
          thumbnail: videoData.snippet.thumbnails.default.url,
          likes: videoData.statistics.likeCount,
          plays: videoData.statistics.viewCount,
          title: videoData.snippet.title,
          duration: videoData.contentDetails.duration,
          creationDate: videoData.snippet.publishedAt,
          starred: false,
          embed: self.createEmbedCode(videoData.id)
        };
      }, (error): Error=>{
        return Error('Service problem');
      });
  }

  /**
   * @param ID
   * @returns {string}
   */
  private static createVideoUrl(ID: string): string {
    return YouTube.URL_VIDEO.replace('$VID$', ID).replace('$KEY$', YouTube.API_KEY);
  }
  
  /**
   * @param URL
   * @returns {boolean}
   */
  validateUrl(url: string): boolean {
    return this.validator.validate(url);
  }
  
  /**
   * @param ID
   * @returns {string}
   */
  createEmbedCode(ID:string){
    return '<iframe src="https://www.youtube.com/embed/'+ID+'" frameborder="0" allowfullscreen></iframe>';
  }

}