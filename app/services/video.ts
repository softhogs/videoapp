import Vimeo from './vimeo';
import YouTube from './youtube';
import {IVideo} from './videoInterface';
import {IVideoItem} from './videoItemInterface';

export default class Video implements IVideo<any>
{

  private services: IVideo<any>[] = [];

  static $inject = ['$http', '$q'];

  constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    this.services.push(new YouTube($http, $q));
    this.services.push(new Vimeo($http, $q));
  }

  fetchVideo(url: string): ng.IPromise<IVideoItem> {
    for (let service of this.services) {
      if (service.validateUrl(url)) {
        return service.fetchVideo(url);
      }
    }
  }

  validateUrl(videoURL: string): boolean {
    for (let service of this.services) {
      if (service.validateUrl(videoURL)) {
        return true;
      }
    }

    return false;
  }

}