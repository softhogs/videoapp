export interface IVideoValidator {
  validate(s: string): boolean;
  getId(s: string): string;
}

export class YouTubeValidator implements IVideoValidator {

  private youTubeUrlRegexp = /(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?/i;
  private youTubeIdRegexp = /([A-z0-9_]{11})/;

  validate(s: string) {
    return this.youTubeUrlRegexp.test(s) || this.youTubeIdRegexp.test(s) || false;
  }
  
  getId(s: string) {
    var ids = this.youTubeUrlRegexp.exec(s) || this.youTubeIdRegexp.exec(s) || null;
    return (ids) ? ids[1] : '';  
  }  
}

export class VimeoValidator implements IVideoValidator {

  private vimeoIdRegexp = /([0-9]{9})/;
  private vimeoUrlRegexp = /vimeo.*\/(\d+)/i;

  validate(s: string) {
    return this.vimeoUrlRegexp.test(s) || this.vimeoIdRegexp.test(s) || false;
  }

  getId(s: string) {
    var ids = this.vimeoUrlRegexp.exec(s) || this.vimeoIdRegexp.exec(s) || null;
    return (ids) ? ids[1] : ''; 
  }
  
}
